import {Component, OnInit} from '@angular/core';
import {Trip} from '../shared/model/trip';
import {Continent} from '../shared/model/continent';
import {Country} from '../shared/model/country';
import {ContinentService} from '../shared/service/continent.service';
import {TripService} from '../shared/service/trip.service';
import {CountryService} from '../shared/service/country.service';
import {City} from '../shared/model/city';
import {CityService} from '../shared/service/city.service';
import {HotelService} from '../shared/service/hotel.service';
import {Hotel} from '../shared/model/hotel';
import {Airport} from '../shared/model/airport';
import {AirportService} from '../shared/service/airport.service';
import {Location} from '@angular/common';

@Component({
  selector: 'app-trip-add',
  templateUrl: './trip-add.component.html',
  styleUrls: ['./trip-add.component.css']
})
export class TripAddComponent implements OnInit {

  trip: Trip = new Trip();
  continents: Continent[] = [];

  country: Country = new Country();
  countries: Country[] = [];

  city: City = new City();
  cities: City[] = [];

  hotel: Hotel = new Hotel();
  hotels: Hotel[] = [];

  airport: Airport = new Airport();
  airports: Airport[] = [];

  typs: string[] = ['HB', 'BB', 'FB', 'AL']

  constructor(private cityService: CityService, private continentService: ContinentService,
              private tripService: TripService, private countryService: CountryService,
              private hotelServices: HotelService, private airportServices: AirportService, private location: Location
  ) {
  }

  ngOnInit() {

    this.continentService.getAllContinent().subscribe(value => this.continents = value);

  }

  onSaveTrip() {
    this.tripService.save(this.trip);
  }

  loadHotels(cityId: number) {
    this.hotelServices.getHotelByCityId(cityId).subscribe(value => this.hotels = value);
  }

  loadCities(countryId: number) {
    this.cityService.getCityByCountryId(countryId).subscribe(value => this.cities = value);
  }

  loadCountries(continentId: number) {
    this.countryService.getCountryByContinentId(continentId).subscribe(value => this.countries = value);
  }

  loadAirport(cityId: number) {
    this.airportServices.getAirportsByCityId(cityId).subscribe((value => this.airports = value));
  }

  goBack(): void {
    this.location.back();
  }

}

