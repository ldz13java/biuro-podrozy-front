import {Component, OnInit} from '@angular/core';
import {CountryService} from '../shared/service/country.service';
import {Country} from '../shared/model/country';
import {Continent} from '../shared/model/continent';
import {Location} from '@angular/common';
import {ContinentService} from '../shared/service/continent.service';
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-country-add',
  templateUrl: './country-add.component.html',
  styleUrls: ['./country-add.component.css']
})
export class CountryAddComponent implements OnInit {
  country: Country = new Country();
  countries: Country[] = [];

  continents: Continent[] = [];

  constructor(private countryService: CountryService, private continentService: ContinentService,
              private  location: Location, private route: ActivatedRoute, private router: Router) {
  }

  ngOnInit() {
    this.countryService.getAllCountries().subscribe(value => this.countries = value);
    this.continentService.getAllContinent().subscribe(value => this.continents = value);

    const idParam = this.route.snapshot.paramMap.get('id');
    const id: number = parseInt(idParam, 10);
    if (id) {
      this.countryService.getCountry(id).subscribe(value => {
        console.log(value);
        this.country = value;
        this.country.id = id;
      });
    }
  }

  onSaveCountry() {
    console.log(this.country);
    this.countryService.save(this.country);
  }

  goBack(): void {
    this.location.back();
  }
}
