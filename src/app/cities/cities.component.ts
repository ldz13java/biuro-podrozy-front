import {Component, OnInit} from '@angular/core';
import {CityService} from '../shared/service/city.service';
import {City} from '../shared/model/city';
import {Location} from '@angular/common';


@Component({
  selector: 'app-cities',
  templateUrl: './cities.component.html',
  styleUrls: ['./cities.component.css']
})
export class CitiesComponent implements OnInit {

  constructor(private cityService: CityService, private location: Location) {
  }

  cities: City[] = [];

  ngOnInit() {
    this.cityService.getAllCities().subscribe(value => this.cities = value);
  }

  goBack(): void {
    this.location.back();
  }

}
