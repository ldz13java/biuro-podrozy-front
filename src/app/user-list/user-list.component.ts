import { Component, OnInit } from '@angular/core';
import {User} from '../shared/model/user';
import {UserServiceService} from '../shared/service/user-service.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  constructor(private userService: UserServiceService) { }
  users: User[] = new Array();

  ngOnInit() {
    this.userService.getAllUsers().subscribe(value => {
      this.users = value;
    });

  }
}
