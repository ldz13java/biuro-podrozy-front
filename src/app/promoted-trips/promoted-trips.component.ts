import {Component, OnInit} from '@angular/core';


@Component({
  selector: 'app-promoted-trips',
  templateUrl: './promoted-trips.component.html',
  styleUrls: ['./promoted-trips.component.css']

})

export class PromotedTripsComponent implements OnInit {

  constructor() {

  }

  trips = new Array<Trip>();
  tripsAzja = new Array<Trip>();
  showClick = true;
  savedKids = 0;
  model;


  saveValue(event) {
    this.showClick = false;
    this.savedKids = event.target.value;
  }

  ngOnInit() {
    this.trips.push(new Trip('New Jork', 1999, '24.04.2019-30.04.2019', 'BB', 'assets/img/basen.jpg'),
      new Trip('Berlin', 3000, '13.05.2019-20.05.2019', 'HB', 'assets/img/gora.jpg'),
      new Trip('Malediwy', 13000, '01.04.2019-23.04.2019', 'All inclusive', 'assets/img/maldives-2122547_1920.jpg'));

    this.tripsAzja.push(new Trip('Malediwy', 8000, '24.04.2019-30.04.2019', 'BB', 'assets/img/maldives-2122547_1920.jpg'),
      new Trip('Malediwy - dla każdego', 9000, '13.05.2019-20.05.2019', 'All inclusive', 'assets/img/maldives-2122547_1920.jpg'),
      new Trip('Malediwy - VIP Trip', 13000, '01.04.2019-23.04.2019', 'All inclusive', 'assets/img/maldives-2122547_1920.jpg'));


  }
}

class Trip {
  constructor(public town: string, public price: number, public date: string, public food: string, public picture: string) {
  }
}
