export class Trip {
  id: number;
  tripName: string;
  contId: number; // klucz obcy
  countryId: number;
  cityId: number;
  hotelToId: number;
  countOfPeople: number;
  description: string;
  picture: string;
  airportId: string;
  price: number;
  typ: string;
}
