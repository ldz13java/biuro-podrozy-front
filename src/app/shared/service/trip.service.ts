import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Trip} from '../model/trip';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TripService {

  constructor(private http: HttpClient) {
  }

  getAll(): Observable<Trip[]> {
    return this.http.get<Trip[]>('http://localhost:8086/rest/trips/all');
  }

  save(trip: Trip) {
    if (trip.id == null) {
      this.http.post('http://localhost:8086/rest/trips/save', trip).subscribe(value => {
        console.log(value);
        window.location.href = '/trips';
      });
    } else {
      this.http.put('http://localhost:8086/rest/trips/modify', trip).subscribe(value => {
        console.log(value);
        window.location.href = '/trips';
      });
    }
  }
}
