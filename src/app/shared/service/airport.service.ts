import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Airport} from '../model/airport';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AirportService {

  constructor(private http: HttpClient) { }

  getAllAirport(): Observable<Airport[]> {
   return this.http.get<Airport[]>('http://localhost:8086/rest/airports/all');

  }

  getAirportsByCityId(cityId: number): Observable<Airport[]> {
    return  this.http.get<Airport[]>('http://localhost:8086/rest/airports/airportsByCity?cityId=' + cityId);
  }

  getAiport(id: number): Observable<Airport> {
    return this.http.get<Airport>('http://localhost:8888/user/' + id);
  }

  save(airport: Airport) {
    if (airport.id == null) {
      this.http.post('http://localhost:8086/rest/airports/save', airport).subscribe(value => {
        console.log(value);
        window.location.href = '/airports';
      });
    } else {
      this.http.put('http://localhost:8086/rest/airports/update', airport).subscribe(value => {
        console.log(value);
        window.location.href = '/airports';
      });
    }
  }

}
