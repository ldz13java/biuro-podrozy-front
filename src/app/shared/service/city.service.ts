import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {City} from '../model/city';


@Injectable({
  providedIn: 'root'
})
export class CityService {

  constructor(private http: HttpClient) {
  }

  getAllCities(): Observable<City[]> {
    return this.http.get<City[]>('http://localhost:8086/rest/cities/all');
  }

  getCityByCountryId(countryId: number): Observable<City[]> {
    return  this.http.get<City[]>('http://localhost:8086/rest/cities/citiesByCountry?countryId=' + countryId);
  }

  getCity(id: number): Observable<City> {
    return this.http.get<City>('http://localhost:8086/rest/cities/' + id);
  }

  save(city: City): void {
    if (city.id == null) {
      this.http.post('http://localhost:8086/rest/cities/save', city).subscribe(value => {
        console.log(value);
        window.location.href = '/cities';
      });
    } else {
      this.http.put('http://localhost:8086/rest/cities/update', city).subscribe(value => {
        console.log(value);
      });
    }
  }

}
