import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Continent} from '../model/continent';

@Injectable({
  providedIn: 'root'
})
export class ContinentService {

  constructor(private  http: HttpClient) {
  }

  getAllContinent(): Observable<Continent[]> {
    return this.http.get<Continent[]>('http://localhost:8086/rest/continents/all');
  }

  save(continent: Continent): void {
    if (continent.id == null) {
      this.http.post('http://localhost:8086/rest/continents/save', continent).subscribe(value => {
        console.log(value);
        window.location.href = '/continents';
      });
    } else {
      this.http.put('http://localhost:8086/rest/continents/modify', continent).subscribe(value => {
        console.log(value);
        window.location.href = '/continents';
      });
    }
  }



  getContinent(id: number): Observable<Continent> {
    return this.http.get<Continent>('http://localhost:8086/rest/continents/' + id);
  }
}
