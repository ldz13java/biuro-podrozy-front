import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Hotel} from '../model/hotel';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HotelService {

  constructor(private http: HttpClient) {
  }


  getAllHotel(): Observable<Hotel[]> {
    return this.http.get<Hotel[]>('http://localhost:8086/rest/hotels/all');
  }

  getHotelByCityId(cityId: number): Observable<Hotel[]> {
    return this.http.get<Hotel[]>('http://localhost:8086/rest/hotels/hotelsByCity?cityId=' + cityId);
  }

  save(hotel: Hotel) {
    if (hotel.id == null) {
      this.http.post('http://localhost:8086/rest/hotels/save', hotel).subscribe(value => {
        console.log(value);
        window.location.href = '/hotels';
      });
    } else {
      this.http.put('http://localhost:8086/rest/hotels/update', hotel).subscribe(value => {
        console.log(value);
      });
    }
  }

  getHotel(id: number): Observable<Hotel> {
    return this.http.get<Hotel>('http://localhost:8086/rest/hotels/' + id);
  }
}
