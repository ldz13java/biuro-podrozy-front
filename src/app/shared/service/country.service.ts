import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Country} from '../model/country';

@Injectable({
  providedIn: 'root'
})
export class CountryService {

  constructor(private  http: HttpClient) {
  }

  getAllCountries(): Observable<Country[]> {
    return this.http.get<Country[]>('http://localhost:8086/rest/countries/all');
  }

  getCountryByContinentId(continentId: number): Observable<Country[]> {
    return this.http.get<Country[]>('http://localhost:8086/rest/countries/countriesByContinents?continentId=' + continentId);
  }
  getCountry(countryId: number): Observable<Country> {
    return this.http.get<Country>('http://localhost:8086/rest/countries/' + countryId);
  }

  save(country: Country): void {
    if (country.id == null) {
      this.http.post('http://localhost:8086/rest/countries/save', country).subscribe(value => {
        console.log(value);
        window.location.href = '/countries';
      });
    } else {
      this.http.put('http://localhost:8086/rest/countries/modify', country).subscribe(value => {
        console.log(value);
        window.location.href = '/countries';
      });
    }
  }
}

