import {Component, OnInit} from '@angular/core';
import {Airport} from '../shared/model/airport';
import {City} from '../shared/model/city';
import {AirportService} from '../shared/service/airport.service';
import {CityService} from '../shared/service/city.service';
import {Location} from '@angular/common';

@Component({
  selector: 'app-airport',
  templateUrl: './airport.component.html',
  styleUrls: ['./airport.component.css']
})
export class AirportComponent implements OnInit {

  airport: Airport = new Airport();
  airports: Airport[] = [];

  cities: City[] = [];

  constructor(private airportServices: AirportService, private cityServices: CityService,
              private location: Location) {
  }

  ngOnInit() {
    this.airportServices.getAllAirport().subscribe(value => this.airports = value);
    this.cityServices.getAllCities().subscribe(value => this.cities = value);
  }

  goBack(): void {
    this.location.back();
  }

}
