import { Component, OnInit } from '@angular/core';
import {Hotel} from '../shared/model/hotel';
import {HotelService} from '../shared/service/hotel.service';
import {Location} from '@angular/common';

@Component({
  selector: 'app-hotels',
  templateUrl: './hotels.component.html',
  styleUrls: ['./hotels.component.css']
})
export class HotelsComponent implements OnInit {
hotels: Hotel[] = [];


  constructor(private hotelService: HotelService, private location: Location) { }

  ngOnInit() {
    this.hotelService.getAllHotel().subscribe(value => this.hotels = value);

  }


  goBack() {
    this.location.back();
  }
}
