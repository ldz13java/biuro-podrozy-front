import {Component, OnInit} from '@angular/core';
import {HotelService} from '../shared/service/hotel.service';
import {Hotel} from '../shared/model/hotel';
import {City} from '../shared/model/city';
import {CityService} from '../shared/service/city.service';
import {Location} from '@angular/common';
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-hotel-add',
  templateUrl: './hotel-add.component.html',
  styleUrls: ['./hotel-add.component.css']
})
export class HotelAddComponent implements OnInit {
  hotel: Hotel = new Hotel();
  hotels: Hotel[] = [];
  cities: City[] = [];
  standards: string[] = ['LOW', 'HIGHT', 'VIP'];
  constructor(private hotelService: HotelService, private cityService: CityService, private location: Location,
              private route: ActivatedRoute, private router: Router) {
  }

  ngOnInit() {
    this.hotelService.getAllHotel().subscribe(value => this.hotels = value);
    this.cityService.getAllCities().subscribe(value => this.cities = value);


    const idParam = this.route.snapshot.paramMap.get('id');
    const id: number = parseInt(idParam, 10);
    if (id) {
      this.hotelService.getHotel(id).subscribe(value => {
        console.log(value);
        this.hotel = value;
        this.hotel.id = id;
      });
    }
  }

  onSaveHotel() {
    this.hotelService.save(this.hotel);
  }

  goBack(): void {
    this.location.back();
  }
}
