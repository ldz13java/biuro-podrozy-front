import {Component, OnInit} from '@angular/core';
import {City} from '../shared/model/city';
import {CityService} from '../shared/service/city.service';
import {Country} from '../shared/model/country';
import {CountryService} from '../shared/service/country.service';
import {Location} from '@angular/common';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-city-add',
  templateUrl: './city-add.component.html',
  styleUrls: ['./city-add.component.css']
})
export class CityAddComponent implements OnInit {
  city: City = new City();
  cities: City[] = [];

  country: Country = new Country();
  countries: Country[] = [];

  constructor(private cityService: CityService, private countryService: CountryService, private location: Location,
              private route: ActivatedRoute, private router: Router) {
  }

  ngOnInit() {
    this.cityService.getAllCities().subscribe(value => this.cities = value);
    this.countryService.getAllCountries().subscribe(value => this.countries = value);

    const idParam = this.route.snapshot.paramMap.get('id');
    const id: number = parseInt(idParam, 10);
    if (id) {
      this.cityService.getCity(id).subscribe(value => {
        console.log(value);
        this.city = value;
        this.city.id = id;
      });
    }
  }
  onSaveCity() {
    this.cityService.save(this.city);


  }

  goBack(): void {
    this.location.back();
  }
}
