import {Component, OnInit} from '@angular/core';
import {Airport} from '../shared/model/airport';
import {AirportService} from '../shared/service/airport.service';
import {CityService} from '../shared/service/city.service';
import {City} from '../shared/model/city';
import {Location} from '@angular/common';
import {ActivatedRoute, Router} from '@angular/router';


@Component({
  selector: 'app-airport-add',
  templateUrl: './airport-add.component.html',
  styleUrls: ['./airport-add.component.css']
})
export class AirportAddComponent implements OnInit {
  airport: Airport = new Airport();
  airports: Airport[] = [];
  cities: City[] = [];

  constructor(private airportService: AirportService, private cityService: CityService,
              private location: Location, private route: ActivatedRoute, private router: Router) {
  }

  ngOnInit() {
    this.cityService.getAllCities().subscribe(value => this.cities = value);
    this.airportService.getAllAirport().subscribe(value => this.airports = value);

    const idParam = this.route.snapshot.paramMap.get('id');
    const id: number = parseInt(idParam, 10);
    if (id) {
      this.airportService.getAiport(id).subscribe(value => {
        console.log(value);
        this.airport = value;
        this.airport.id = id;
      });
    }
  }

  onSaveAirport() {
    this.airportService.save(this.airport);
  }
  goBack(): void {
    this.location.back();
  }



}
