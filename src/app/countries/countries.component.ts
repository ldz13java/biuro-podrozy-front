import {Component, OnInit} from '@angular/core';
import {CountryService} from '../shared/service/country.service';
import {Country} from '../shared/model/country';
import {Location} from "@angular/common";

@Component({
  selector: 'app-countries',
  templateUrl: './countries.component.html',
  styleUrls: ['./countries.component.css']
})
export class CountriesComponent implements OnInit {
  constructor(private countryService: CountryService, private location: Location) {
  }

  country: Country = new Country();
  countries: Country[] = new Array();

  ngOnInit() {
    this.countryService.getAllCountries().subscribe(value => this.countries = value);

  }

  goBack(): void {
    this.location.back();
  }

}
