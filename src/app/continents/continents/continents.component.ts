import {Component, OnInit} from '@angular/core';
import {ContinentService} from '../../shared/service/continent.service';
import {Continent} from '../../shared/model/continent';
import {Location} from '@angular/common';

@Component({
  selector: 'app-continents',
  templateUrl: './continents.component.html',
  styleUrls: ['./continents.component.css']
})
export class ContinentsComponent implements OnInit {

  constructor(private continentService: ContinentService, private location: Location) {
  }

  continents: Continent[] = new Array();

  ngOnInit() {
    this.continentService.getAllContinent().subscribe(value => this.continents = value);
  }

  goBack(): void {
    this.location.back();
  }

}
