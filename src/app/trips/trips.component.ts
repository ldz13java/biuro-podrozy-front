import { Component, OnInit } from '@angular/core';
import {Trip} from '../shared/model/trip';
import {TripService} from '../shared/service/trip.service';
import {Location} from '@angular/common';


@Component({
  selector: 'app-trips',
  templateUrl: './trips.component.html',
  styleUrls: ['./trips.component.css']
})
export class TripsComponent implements OnInit {

  trip: Trip = new Trip();
  trips: Trip[] = [];
  constructor(private tripService: TripService, private location: Location) { }

  ngOnInit() {
    this.tripService.getAll().subscribe(value => this.trips = value);

  }

  goBack(): void {
    this.location.back();
  }
}
