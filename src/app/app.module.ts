import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { UserListComponent } from './user-list/user-list.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AppRoutingModule } from './app-routing.module';
import {RouterModule} from '@angular/router';
import {UserServiceService} from './shared/service/user-service.service';
import {HttpClientModule} from '@angular/common/http';
import { ContinentsComponent } from './continents/continents/continents.component';
import { PromotedTripsComponent } from './promoted-trips/promoted-trips.component';
import { AdminpanelComponent } from './adminpanel/adminpanel.component';
import {FormsModule} from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {CountriesComponent} from './countries/countries.component';
import { CityAddComponent } from './city-add/city-add.component';
import { CountryAddComponent } from './country-add/country-add.component';
import {TripAddComponent} from './trip-add/trip-add.component';
import { CitiesComponent } from './cities/cities.component';
import { HotelAddComponent } from './hotel-add/hotel-add.component';
import { ContinentsAddComponent } from './continents-add/continents-add.component';
import { TripsComponent } from './trips/trips.component';
import { AirportAddComponent } from './airport-add/airport-add.component';
import { AirportComponent } from './airport/airport.component';
import { HotelsComponent } from './hotels/hotels.component';



@NgModule({
  declarations: [
    AppComponent,
    UserListComponent,
    DashboardComponent,
    ContinentsComponent,
    PromotedTripsComponent,
    AdminpanelComponent,
    CountriesComponent,
    CityAddComponent,
    CountryAddComponent,
    TripAddComponent,
    CitiesComponent,
    HotelAddComponent,
    ContinentsAddComponent,
    TripsComponent,
    AirportAddComponent,
    AirportComponent,
    HotelsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    HttpClientModule,
    FormsModule,
    NgbModule

  ],

  providers: [UserServiceService],
  bootstrap: [AppComponent]
})
export class AppModule {}

