import {Component, OnInit} from '@angular/core';


@Component({
  selector: 'app-adminpanel',
  templateUrl: './adminpanel.component.html',
  styleUrls: ['./adminpanel.component.css']
})
export class AdminpanelComponent implements OnInit {

  menuAdmin: Menu[] = [];
  constructor() {
  }


  ngOnInit() {
    this.menuAdmin.push(new Menu('Wycieczki', '/trip-add',  '/trips'),
      new Menu('Kontynenty', '/continents-add',  '/continents'),
      new Menu('Państwa', '/country-add', '/countries'),
      new Menu('Miasta', '/city-add', '/cities'),
      new Menu('Hotele', '/hotel-add', '/hotels'),
      new Menu('Lotniska', '/airport-add', '/airports'));
  }


  onClick() {

  }
}

class Menu {
  constructor(public nameMenu: string, public addMenu: string, public  changeMenu: string) {
  }

}
