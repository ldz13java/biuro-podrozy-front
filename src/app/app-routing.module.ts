import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {UserListComponent} from './user-list/user-list.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {ContinentsComponent} from './continents/continents/continents.component';
import {PromotedTripsComponent} from './promoted-trips/promoted-trips.component';
import {AdminpanelComponent} from './adminpanel/adminpanel.component';
import {CountriesComponent} from './countries/countries.component';
import {CityAddComponent} from './city-add/city-add.component';
import {TripAddComponent} from './trip-add/trip-add.component';
import {CountryAddComponent} from './country-add/country-add.component';
import {CitiesComponent} from './cities/cities.component';
import {HotelAddComponent} from './hotel-add/hotel-add.component';
import {ContinentsAddComponent} from './continents-add/continents-add.component';
import {AirportAddComponent} from './airport-add/airport-add.component';
import {AirportComponent} from './airport/airport.component';
import {HotelsComponent} from './hotels/hotels.component';
import {TripsComponent} from "./trips/trips.component";

const routes: Routes = [
  { path: 'user-list', component: UserListComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'continents', component: ContinentsComponent },
  { path: 'promoted-trips', component: PromotedTripsComponent},
  {path: 'adminpanel', component: AdminpanelComponent},
  {path: 'countries', component: CountriesComponent},
  {path: 'city-add', component: CityAddComponent},
  {path: 'city-add/:id', component: CityAddComponent},
  {path: 'continent-add/:id', component: ContinentsAddComponent},
  {path: 'country-add/:id', component: CountryAddComponent},
  {path: 'country-add', component: CountryAddComponent},
  {path: 'trip-add', component: TripAddComponent},
  {path: 'cities', component: CitiesComponent},
  {path: 'hotel-add', component: HotelAddComponent},
  {path: 'hotel-add/:id', component: HotelAddComponent},
  {path: 'continents-add', component: ContinentsAddComponent},
  {path: 'airport-add', component: AirportAddComponent},
  {path: 'airports', component: AirportComponent},
  {path: 'hotels', component: HotelsComponent},
  {path: 'trips', component: TripsComponent}
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes),
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
