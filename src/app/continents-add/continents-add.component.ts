import {Component, OnInit} from '@angular/core';
import {ContinentService} from '../shared/service/continent.service';
import {Continent} from '../shared/model/continent';
import {Location} from '@angular/common';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-continents-add',
  templateUrl: './continents-add.component.html',
  styleUrls: ['./continents-add.component.css']
})
export class ContinentsAddComponent implements OnInit {
  continent: Continent = new Continent();
  continents: Continent[] = [];

  constructor(private continentService: ContinentService, private location: Location, private route: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit() {
    const idParam = this.route.snapshot.paramMap.get('id');
    const id: number = parseInt(idParam, 10);
    if (id) {
      this.continentService.getContinent(id).subscribe(value => {
        console.log(value);
        this.continent = value;
        this.continent.id = id;
        // this.loadCountry(this.city.countryId);
      });
    }
  }

  onSaveContinent() {
    this.continentService.save(this.continent);

  }

  goBack() {
    this.location.back();
  }
}
